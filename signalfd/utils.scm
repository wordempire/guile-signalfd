;;; guile-signalfd --- Semantic Version tooling for guile
;;; Copyright © 2017 Jelle Dirk Licht <jlicht@fsfe.org>
;;;
;;; This file is part of guile-signalfd.
;;;
;;; guile-signalfd is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; guile-signalfd is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with guile-signalfd.  If not, see <http://www.gnu.org/licenses/>.

(define-module (signalfd utils)
  #:use-module ((system foreign) #:prefix ffi:)
  #:use-module (oop goops)
  #:use-module (ice-9 match)
  #:use-module (ice-9 binary-ports)
  #:use-module (rnrs bytevectors)
  #:use-module (bytestructures guile)
  #:use-module (bytestructures guile ffi))

;;(define my-position-struct (bs:struct `((x ,int) (y ,int))))

(define SFD_NONBLOCK #o00004000)
(define SFD_CLOEXEC #o02000000)

(define SIG_BLOCK 0)
(define SIG_UNBLOCK 1)
(define SIG_SETMASK 2)

(define libc (dynamic-link))

(define (libc->procedure return name params)
  (ffi:pointer->procedure return (dynamic-func name libc) params))

(define %signalfd-siginfo-struct ; sigset_t
  (bytestructure-descriptor->ffi-descriptor
   (bs:struct `((ssi-signo ,uint32)
                (ssi-errno ,int32)
                (ssi-code ,int32)
                (ssi-pid ,uint32)
                (ssi-fd ,int32)
                (ssi-tid ,uint32)
                (ssi-band ,uint32)
                (ssi-overrun ,uint32)
                (ssi-trapno ,uint32)
                (ssi-status ,int32)
                (ssi-int ,int32)
                (ssi-ptr ,uint64)
                (ssi-utime ,uint64)
                (ssi-stime ,uint64)
                (ssi-add ,uint64)
                (pad ,(bs:vector 48 uint8))))))

(define (make-signalfd-siginfo) ;; o.m.g. so ugly
  (ffi:make-c-struct %signalfd-siginfo-struct
                     (append
                      (make-list 15 0)
                      `(,(make-list 48 0)))))

(define %sigset-size 32)

(define %sigset-struct ; sigset_t
  (bytestructure-descriptor->ffi-descriptor
   (bs:struct `((pad ,(bs:vector %sigset-size uint64))))))
   ;; (bs:struct
   ;;  `((ssi-signo ,uint32)
   ;;              (ssi-errno ,int32)
   ;;              (ssi-code ,int32)
   ;;              (ssi-pid ,uint32)
   ;;              (ssi-fd ,int32)
   ;;              (ssi-tid ,uint32)
   ;;              (ssi-band ,uint32)
   ;;              (ssi-overrun ,uint32)
   ;;              (ssi-trapno ,uint32)
   ;;              (ssi-status ,int32)
   ;;              (ssi-int ,int32)
   ;;              (ssi-ptr ,uint64)
   ;;              (ssi-utime ,uint64)
   ;;              (ssi-stime ,uint64)
   ;;              (ssi-add ,uint64)
   ;;              (pad ,(bs:vector 48 uint8))))))
  ;;(list ffi:int)
  ;;(bytestructure-descriptor->ffi-descriptor (bs:vector 1 uint64))


(define (sigset-content sigset)
  (match (ffi:parse-c-struct sigset %sigset-struct)
    ((set)
     set)))

;; Signals from 1 ... 64 inclusive

;;int sigemptyset(sigset_t *set);
(define _sigemptyset
  (libc->procedure ffi:int "sigemptyset" (list '*)))

(define (make-sigset)
  (let ((set (ffi:make-c-struct %sigset-struct (list (make-list %sigset-size 0)))))
    (_sigemptyset set)
    set))

;;int sigfillset(sigset_t *set);
(define _sigfillset
  (libc->procedure ffi:int "sigfillset" (list '*)))

;;int sigaddset(sigset_t *set, int signum);
(define _sigaddset
  (libc->procedure ffi:int "sigaddset" (list '* ffi:int)))
;;int sigdelset(sigset_t *set, int signum);
(define _sigdelset
  (libc->procedure ffi:int "sigdelset" (list '* ffi:int)))
;;int sigismember(const sigset_t *set, int signum);
(define _sigismember
  (libc->procedure ffi:int "sigismember" (list '* ffi:int)))
;;       int sigprocmask(int how, const sigset_t *set, sigset_t *oldset);
(define _sigprocmask
  (libc->procedure ffi:int "pthread_sigmask" (list ffi:int '* '*)))
  ;;(libc->procedure ffi:int "sigprocmask" (list ffi:int '* '*)))
;; int signalfd(int fd, const sigset_t *mask, int flags);
(define _signalfd
  (libc->procedure ffi:int "signalfd" (list ffi:int '* ffi:int)))

(define _read
  (libc->procedure ffi:size_t "read" (list ffi:int '* ffi:size_t)))


(define-class <sigset> ()
  (sigset #:init-keyword #:sigset
          #:init-form (make-sigset)
          #:getter sigset-of))
;; empty fill add remove ismember
(define-method (empty (obj <sigset>))
  (_sigemptyset (sigset-of obj)))

(define-method (fill (obj <sigset>))
  (_sigfillset (sigset-of obj)))

(define-method (add (obj <sigset>) (signal <integer>))
  (_sigaddset (sigset-of obj) signal))

(define-method (remove (obj <sigset>) (signal <integer>))
  (_sigdelset (sigset-of obj) signal))

(define-method (member? (obj <sigset>) (signal <integer>))
  (not (zero? (_sigismember (sigset-of obj) signal))))

(define-method (sigprocmask (obj <sigset>) (mode <integer>))
  (let* ((oldsignals (make-sigset))

         (res (_sigprocmask mode (sigset-of obj) oldsignals)))
    (if (negative? res)
        (throw 'yolo-error "sigprocmask")
        (make-instance <sigset> #:sigset oldsignals))))

;; return sigset(oldsignals)
;; (define* (sigprocmask signals #:optional (mode SIG_SETMASK))
;;   (let* ((oldsignals (make-sigset))
;;          (res (_sigprocmask mode (sigset-of signals) oldsignals)))
;;     (if (negative? res)
;;         (throw 'yolo-error "sigprocmask")
;;         (make-instance <sigset> #:sigset oldsignals))))


;; TODO: some kind of object for storing signals/fds/oldsigsets

(define-class <signalfd> ()
  (signals #:init-keyword #:sigset
           ;;#:init-value (make-sigset)
           #:getter sigset-of)
  (old-signals)
  (flags #:init-keyword #:flags
         #:init-value 0 ;;SFD_NONBLOCK
         #:getter flags-of)
  (signal-port #:getter port-of))


(define-method (my/open (obj <signalfd>))
  (let ((res (_signalfd -1 ;; no existing FD
                        (sigset-of (sigset-of obj)) ;;TODO: better names
			0
			)))
    (if (negative? res)
        (throw 'signfalfd-open-error "Something went wrong")
        (slot-set! obj 'signal-port res;;(fdes->inport res)
		   ))))

(define-method (my/use (obj <signalfd>))
  (slot-set! obj 'old-signals
             (sigprocmask (sigset-of obj)
                          SIG_BLOCK)))

(define-method (my/close (obj <signalfd>))
  (close (port-of obj)))

(define-method (drop (obj <signalfd>))
  ;;(sigprocmask (slot-ref obj 'old-signals) SIG_SETMASK)
  (my/close obj)
  #f)

(define (make-info)
  ;;(make
  (make-bytevector (ffi:sizeof %signalfd-siginfo-struct) 0)
  )

(define-method (info (obj <signalfd>))
	       ;;(setvbuf (port-of obj) _IONBF)
	       	;;(let ((flags (fcntl (port-of obj) F_GETFL)))
  	;;(fcntl (port-of obj) F_SETFL (logior O_NONBLOCK flags)))

	       (display "swag")
  (let* ((ref (make-info))
         (bytes  
	   (_read (port-of obj) (ffi:bytevector->pointer ref) (ffi:sizeof %signalfd-siginfo-struct))
	   ;;(get-u8 (port-of obj))
	   ;;(get-bytevector-n (port-of obj)
                                  ;;(ffi:sizeof %signalfd-siginfo-struct)
				  ;;)
	   ) ;;  todo: should be while-loop, because this consumer _ALL_ pending events
         ;;(res (ffi:parse-c-struct (ffi:bytevector->pointer bytes)  %signalfd-siginfo-struct))
         )
    bytes
  ))

(define (bigtest SIGNAL)
  (let* ((mysigset (make-instance <sigset>))
         (_ (add mysigset SIGNAL))
         (mysigfd (make-instance <signalfd> #:sigset mysigset)))
    (my/use mysigfd)
    (my/open mysigfd)
    (display "waiting...")
    (let ((infos (info mysigfd)))
      (drop mysigfd)
      infos)
    ))

;; This stuff works! :D
(define (signal-blocked? signal)
  (let* ((mysigset (make-sigset)))
    (_sigprocmask -1 ffi:%null-pointer mysigset)
    (_sigismember mysigset signal)))

(define (reset-proc-mask)
  (let* ((mysigset (make-sigset)))
    (_sigemptyset mysigset)
    (_sigprocmask SIG_SETMASK mysigset ffi:%null-pointer)
    ))

(define (create-mask signals)
  (let ((mysigset (make-sigset)))
    (_sigemptyset mysigset)
    (for-each (lambda (signal)
                (_sigaddset mysigset signal)) signals)
    mysigset))

(define (block-signals signals)
  (let ((mysigset (create-mask signals)))
    (_sigprocmask SIG_BLOCK mysigset ffi:%null-pointer)))

;;
;; (let ((res (_signalfd -1 ;; no existing FD
;;                       (sigset-of (sigset-of obj)) ;;TODO: better names
;;                       0
;;                       )))
;;   (if (negative? res)
;;       (throw 'signfalfd-open-error "Something went wrong")
;;       (slot-set! obj 'signal-port res;;(fdes->inport res)
(define (signalfd signals)
  (let* ((mysigset (create-mask signals))
         (res (_signalfd -1 mysigset 0)))
    (if (negative? res)
        (throw 'signalfd-open-error "Something went wrong")
        (fdes->inport res))))

(define (read-info port)
  (let* ((bytes (get-bytevector-n port (ffi:sizeof %signalfd-siginfo-struct)))
         (res (ffi:parse-c-struct (ffi:bytevector->pointer bytes)  %signalfd-siginfo-struct)))
    res))

(define (demo)
  (let* ((signals (list SIGCHLD)))
    (sigaction SIGCHLD (lambda _ (display "caught signal\n")))
    (block-signals signals)
    (while #t
      (display "blocked: ")
      (display (signal-blocked? SIGCHLD))
      (display "... waking...\n")
      (raise SIGCHLD)
      (sleep 1))
    )
  )
;;(demo)
;; End working stuff

;;(define (signalfd-signals))


(define (super)
  (let* ((mysig (make-sigset)))
    (display "threads:")
    (display (all-threads))
    (display "\nCurrent thread:")
    (display (current-thread))
    (display "\n")
    ;;(system* "./signals")
    (_sigaddset mysig SIGCHLD)
    (_sigaddset mysig SIGINT)
    ;;(_sigfillset mysig)
    ;;(display (sigset-content mysig))
    (_sigprocmask SIG_SETMASK mysig ffi:%null-pointer)
    ;;(system* "./signals-infinite")
    (sigaction SIGCHLD (lambda _
                         (display "lolwut, in handler:")
                         (display (current-thread))
                         (display "\n")
                               ))
    ;;(while #t
      (display "waking\n")
      (sleep 1))
    ;;)
  )
;;(super)

;;(bigtest 17)
;;(display "hi")
;; (display (ffi:sizeof %signalfd-siginfo-struct))
