;;; guile-signalfd --- Semantic Version tooling for guile
;;; Copyright © 2017 Jelle Dirk Licht <jlicht@fsfe.org>
;;;
;;; This file is part of guile-signalfd.
;;;
;;; guile-signalfd is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; guile-signalfd is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with guile-signalfd.  If not, see <http://www.gnu.org/licenses/>.

(define-module (signalfd constants)
  #:use-module (signalfd utils)
  #:export (
            SFD_NONBLOCK
            SFD_CLOEXEC

            SIG_BLOCK
            SIG_UNBLOCK
            SIG_SETMASK
            ))

;; SFD_NONBLOCK = 00004000
;; SFD_CLOEXEC = 02000000

;; SIG_BLOCK   = 0
;; SIG_UNBLOCK = 1
;; SIG_SETMASK = 2
(use-modules (rnrs bytevectors))
(define SFD_NONBLOCK #o00004000)
(define SFD_CLOEXEC #o02000000)

(define SIG_BLOCK 0)
(define SIG_UNBLOCK 1)
(define SIG_SETMASK 2)
